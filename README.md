# tf-s3

## Overview

This is an example of using terraform workspaces in a single repo.

This project consist of a Makefile, which uses docker to plan and triggers docker shell for manual terraform actions such as taint/import/etc.

The workflow is supported by Atlantis.

## TLDR

### Typical workflow

- `make plan env=<env> region=<region>`: plan the project base on `${env}` and `${region}` as workspace
- `make shell env=<env> region=<region>`: enter a terraform docker shell with the project base on `${env}` and `${region}` as workspace

### Workspace
The workspace comprise of  `${env}` and `${region}`, e.g if env is `dev` and region is `sg` then the workspace is `dev:sg` 


### Terraform Execution

If execution is via manual, we run `make` commands to bootstrap, Terraform docker container to perform the actions. 
It shall based on the supplied env and region to select the tfvars to load with terraform binary. (See Makefile `init` build)

When Atlantis is use, atlantis will know which workspace is triggered and plans for that particular workspace only if the files of the workspaces changes.

