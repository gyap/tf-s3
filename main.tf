terraform {
  required_version = ">= 1.0.4"

  backend "s3" {
    bucket = var.bucket
    key = var.key
    region = var.region
    encrypt = var.encrypt
    profile = var.profile
    workspace_key_prefix = var.workspace_key_prefix
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = var.region
  profile = var.profile
}

locals {
  # for each file in data/<profile>/<region>:
  #   a) read the contents of the file and decode yaml to map
  #   b) merge the list of maps 
  # duplicates are being merges
  buckets = merge([ for file in fileset(path.module, "data/${var.profile}/${var.region_short}/*.yaml"): yamldecode(file(file)) ]...)
}

# parse map of bucket definition created by local module
module "s3" {
  source = "./module"
  buckets = local.buckets
}

# for debug output
output "debug" {
  value = var.debug ? module.s3.buckets : null
}

# backend 
variable "region" {}
variable "key" {}
variable "profile" {}
variable "bucket" {}
variable "encrypt" {}
variable "workspace_key_prefix" {}

# provider
variable "region_short" {}

# debug
variable "debug" {}
