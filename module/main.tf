module "s3_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "2.13.0"
  # insert the 5 required variables here

  for_each = var.buckets

  bucket = each.key

  acceleration_status = try(each.value.acceleration_status, null )
  bucket_prefix       = try(each.value.bucket_prefix, null )
  policy              = try(each.value.policy, null ) # this need policy drop improvements
  request_payer       = try(each.value.request_payer, null ) 

  # Optional Inputs
  acl                                   = try(each.value.acl, "private" ) 
  attach_deny_insecure_transport_policy = try(each.value.attach_deny_insecure_transport_policy, false ) 
  attach_elb_log_delivery_policy        = try(each.value.attach_elb_log_delivery_policy, false ) 
  attach_lb_log_delivery_policy         = try(each.value.attach_elb_log_delivery_policy, false ) 
  attach_policy                         = try(each.value.attach_policy, false )
  attach_public_policy                  = try(each.value.attach_public_policy, true )
  attach_require_latest_tls_policy      = try(each.value.attach_require_latest_tls_policy, false )
  block_public_acls                     = try(each.value.block_public_acls, false )
  block_public_policy                   = try(each.value.block_public_policy, false )
  control_object_ownership              = try(each.value.control_object_ownership, false )
  cors_rule                             = try(each.value.cors_rule, [] )
  create_bucket                         = try(each.value.create_bucket, true )
  force_destroy                         = try(each.value.force_destroy, false )
  grant                                 = try(each.value.force_destroy, [] )
  ignore_public_acls                    = try(each.value.ignore_public_acls, false )
  lifecycle_rule                        = try(each.value.lifecycle_rule, [] )
  logging                               = try(each.value.logging, {} )
  object_lock_configuration             = try(each.value.object_lock_configuration, {} )
  object_ownership                      = try(each.value.object_ownership, "ObjectWriter" )
  replication_configuration             = try(each.value.replication_configuration, {} )
  restrict_public_buckets               = try(each.value.restrict_public_buckets, false )
  server_side_encryption_configuration  = try(each.value.server_side_encryption_configuration, {} )
  tags                                  = try(each.value.restrict_public_buckets, {} )
  versioning                            = try(each.value.restrict_public_buckets, {} )
  website                               = try(each.value.restrict_public_buckets, {} )
}

variable "buckets" {
  description = "S3 buckets."
  type = any
}

output "buckets" {
  value = module.s3_bucket
}