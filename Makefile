TERRAFORM_VERSION=1.0.10
DOCKER_IMAGE="hashicorp/terraform:${TERRAFORM_VERSION}"

TERRAFORM= docker run --rm -it --name "terraform_${TERRAFORM_VERSION}"  -v "${HOME}"/.aws:/root/.ssh -v "${HOME}"/.aws:/root/.aws  -v ${PWD}:/tf -w /tf ${DOCKER_IMAGE}
TERRAFORM_SHELL= docker run --rm -it --name "terraform_${TERRAFORM_VERSION}" -v "${HOME}"/.aws:/root/.ssh -v "${HOME}"/.aws:/root/.aws -v ${PWD}:/tf -w /tf  --entrypoint="/bin/sh" ${DOCKER_IMAGE}
TERRAFORM_RM= docker run --rm -it --name "terraform_${TERRAFORM_VERSION}" -v "${HOME}"/.aws:/root/.ssh -v "${HOME}"/.aws:/root/.aws -v ${PWD}:/tf -w /tf  --entrypoint="/bin/rm" ${DOCKER_IMAGE}

help:
	@echo "Typical workflow"
	@echo "================"
	@echo "Be a good boy always use atlantis :)"
	@echo ""
	@echo "1. make plan -> to view the current plan manually"
	@echo "2. make shell -> enter terraform docker shell for advance debugging"

clean:
	@echo "cleanup ..."
	@$(TERRAFORM_RM) -rfv .terraform .terraform.lock.hcl

required: 
	@[ "${env}" ] || ( echo "env is not set"; exit 1 )
	@[ "${region}" ] || ( echo "region is not set"; exit 1 )

init: required
	@echo "=== Init ==="
	@echo "terraform init -backend-config=env/${env}/${region}/backend.tfvars"
	@$(TERRAFORM) init -backend-config="env/${env}/${region}/backend.tfvars" 

init-reconfigure: required
	@echo "=== Init (reconfigure) ==="
	@echo "terraform init -reconfigure -backend-config=env/${env}/${region}/backend.tfvars"
	@$(TERRAFORM) init -reconfigure -backend-config="env/${env}/${region}/backend.tfvars" 
	
workspace:
	@echo "=== Workspace ==="
	@[ "${env}" ] || ( echo "env is not set"; exit 1 )
	@[ "${region}" ] || ( echo "region is not set"; exit 1 )
	@$(TERRAFORM) workspace new "$(env):$(region)" || $(TERRAFORM) workspace select "$(env):$(region)"

refresh: init workspace
	@echo "=== Refresh ==="
	@echo "terraform refresh" 
	@$(TERRAFORM) refresh -var-file="env/${env}/${region}/variables.tfvars" -var-file="env/${env}/${region}/backend.tfvars" 

plan: init workspace
	@echo "=== Plan ==="
	@echo "terraform plan -var-file=env/${env}/${region}/variables.tfvars" -var-file="env/${env}/${region}/backend.tfvars" 
	@$(TERRAFORM) plan -var-file="env/${env}/${region}/variables.tfvars" -var-file="env/${env}/${region}/backend.tfvars" 


shell: init  
	@echo "shell"
	@$(TERRAFORM_SHELL)

.PHONY: help clean required init init-reconfigure workspace refresh shell 